This was by far one of the most interesting classes I've taken.
The best way to describe this class in my mind is "learn how computers are exploited so that you can trace or prevent the exploits in the future" This repository just contains a little code but mostly my analysis concerning different topics in computer forensics. You can find my analysis usually in pdf format inside the respective folders.

Lab 01 - practice with unix terminal

Lab 02 - Analyzing a FAT and NTFS file systems

Lab 03 - Retrieving erased evidence from a disk image

Lab 04 - Analyzing executables and writing a C program to alter the flow of logic

Lab 05 - Writing a C program and giving it to other students to reverse-engineer

Lab 06 - Buffer overflow exploits and injecting assembly code

Lab 07 - Packet analysis

Final Lab - Live packet analysis (on private network because it is illegal to sniff packets on a public network)