//Jonathan Zallas
//Computer Forensics

#include <stdio.h>
#include <stdlib.h>


int main ( int argc, char *argv[] )
{
    if ( argc != 2 )
    {
        printf( "usage: %s filename\n", argv[0] );
    }
    else
    {
        FILE *file = fopen( argv[1], "rb+" );

        if ( file == 0 )
        {
            printf( "Could not open file\n" );
        }
        else
        {
            int curr = 0x00;
            int address = 0x58f;
            int jg = 0x7f;
            int jle = 0x7e;

            //go to address
            fseek (file , address , SEEK_SET );
            curr = fgetc (file);

            fseek (file , address , SEEK_SET );

            //output what opcode is set in the program
            if (curr == jg)
            {
                printf("The current opcode is JG, changing the opcode to JLE.\n");
                fputc(jle, file);
            }
            else if (curr == jle)
            {
                printf("The current opcode is JLE, changing the opcode to JG.\n");
                fputc(jg, file);
            }
            else
            {
                printf("The current opcode is unknown, exiting program.\n");
                return 0;
            }

            //close the file
            fclose(file);
            printf( "Edited %s successfully.\n", argv[1]);
        }
    }
    return 0;
}
