#include <stdio.h>
#include <stdlib.h>


int main ( int argc, char *argv[] )
{
    if ( argc != 2 )
    {
        printf( "usage: %s filename\n", argv[0] );
    }
    else
    {
        FILE *file = fopen( argv[1], "rb+" );

        if ( file == 0 )
        {
            printf( "Could not open file\n" );
        }
        else
        {
            int address1 = 0x534; //this has bc, needs c0
            int address2 = 0x53b; // has c0, needs bc
            int bc = 0xbc;
            int c0 = 0xc0;

            //go to address
            fseek (file , address1 , SEEK_SET );
            fputc(c0, file);

            fseek (file , address2 , SEEK_SET );
            fputc(bc, file);

            //close the file
            fclose(file);
            printf( "Edited %s successfully.\n", argv[1]);
        }
    }
    return 0;
}