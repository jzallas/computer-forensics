#include <stdio.h>
#include <string.h>


void outputInBinary(char hex[]){
    const char *binaryKey[16] = {"0000","0001","0010","0011","0100","0110","0101","0111","1000","1001","1010","1011","1100","1101","1110","1111"};
    unsigned int length = (unsigned)strlen(hex);

    int i;
    for(i = 0; i < length ; i++){

        char character = hex[i];

        if(character == 'a')
        {
            //printf("1101");
            printf("%s", binaryKey[10]);
        }
        else if (character == 'b')
            printf("%s", binaryKey[11]);
        else if (character == 'c')
            printf("%s", binaryKey[12]);
        else if (character == 'd')
            printf("%s", binaryKey[13]);
        else if (character == 'e')
            printf("%s", binaryKey[14]);
        else if (character == 'f')
            printf("%s", binaryKey[15]);
        else
        {
            printf("%s", binaryKey[character-'0']);
        }
    }
}

int main ()
{
  char hex[10];
  printf ("Enter a hexadecimal number (max 10 characters): ");
  scanf ("%s", hex);
  printf ("You have entered 0x%s.\nThe binary equivalent is ", hex);
  outputInBinary(hex);
  printf("\n");
  return 0;
}
